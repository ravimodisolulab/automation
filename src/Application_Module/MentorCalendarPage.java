package Application_Module;


import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Reporter;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.TestData_Sheet;

public class MentorCalendarPage extends Actions_Class
{
		public static String classname = "Mentor Calendar Page";
		public static void addAvailability(String startDate, String endDate, String  currentTime, String futureTime) throws Exception
		{
			String handle= driver.getWindowHandle();
			System.out.println(handle);
			
			clickElement("xpath", mcaddAvailability);
			Thread.sleep(5000);
			
			Set<?> handles = driver.getWindowHandles();
			System.out.println(handles);
			
			for (String handle1 : driver.getWindowHandles()) 
			{
		        System.out.println(handle1);
		        
				clickElement("xpath", mcastartDateSlot); 
				Thread.sleep(2000);
				
				clearElement("xpath", mcastartDateSlot);
				sendKeys("xpath", mcastartDateSlot, startDate);
				Thread.sleep(2000);
							
				clickElement("xpath", mcaendDateSlot);
				Thread.sleep(2000);
				clearElement("xpath", mcaendDateSlot);
				sendKeys("xpath", mcaendDateSlot, endDate);
				Thread.sleep(2000);
				
				clickElement("xpath", mcastartTimeSlot);
				Thread.sleep(2000);
				clearElement("xpath", mcastartTimeSlot);
				sendKeys("xpath", mcastartTimeSlot, currentTime);
				Thread.sleep(2000);
				
				clickElement("xpath", mcaendTimeSlot);
				Thread.sleep(2000);
				clearElement("xpath", mcaendTimeSlot);
				sendKeys("xpath", mcaendTimeSlot, futureTime);
				Thread.sleep(10000);
				
				driver.findElement(By.xpath(mcaendTimeSlot)).sendKeys(Keys.ENTER);
				//clickElement("xpath", mcaaddAvailabilityPopupOK);
				Thread.sleep(10000);
			
			
				classname = "Add Avialability - " + startDate + currentTime + endDate + futureTime;
				Success_FailureMessageSheet.storeMessageinfile(classname);
				Thread.sleep(2000);
				System.out.println("Avialability added successfully...");
				Reporter.log(classname);
				
				//driver.switchTo().window(handle1);
	 	      }
			  //driver.switchTo().window(handle1);
		     //driver.close();
		}
		
		public static void sendPrivateInviteToUser(String xlxspath, String startDate, String startTime) throws Exception
		{
			DataSheetFile(xlxspath, "BookMeeting");
			int s =  TestData_Sheet.getCellDataInt(1, 0);
			Thread.sleep(2000);
			String message = TestData_Sheet.getCellData(1, 4);
			Thread.sleep(2000);
			DataSheetFile(xlxspath, "Login");
			String userName = TestData_Sheet.getCellData(2, 3);
			
			String handle= driver.getWindowHandle();
			System.out.println(handle);
						
			clickElement("xpath", mcsendPrivateInvite);
			Thread.sleep(5000);	
			
			Set<?> handles = driver.getWindowHandles();
			System.out.println(handles);
			
			  for (String handle1 : driver.getWindowHandles()) 
			  {
		        	System.out.println(handle1);
		        	
		        	clickElement("xpath", mcpiClickSelectUser);	Thread.sleep(2000);
		        	clearElement("xpath", mcpiSearchUser);Thread.sleep(1000);
		        	sendKeys("xpath", mcpiSearchUser, userName); Thread.sleep(1000);
		        	clickElement("xpath", mcpiSelectUser);
					dropdownByIndex("xpath", mcpiSelectMeetingDuration, s); Thread.sleep(2000);
					clearElement("xpath", mcpiSelectStartDate);	Thread.sleep(1000);
					sendKeys("xpath", mcpiSelectStartDate, startDate);	Thread.sleep(1000);
					clearElement("xpath", mcpiSelectStartTime); Thread.sleep(1000);
					sendKeys("xpath", mcpiSelectStartTime, startTime); Thread.sleep(1000);
					clearElement("xpath", mcpiMessage); Thread.sleep(1000);
					sendKeys("xpath",mcpiMessage, message); Thread.sleep(1000);
					clickElement("xpath", mcpiSendInvite);
					Thread.sleep(8000);
					
					Reporter.log("Mentor successfully sent private invite to user. Name is:-  " + userName);
					
		        	//driver.switchTo().window(handle1);
	 	      }
			  //driver.switchTo().window(handle1);
		     //driver.close();
		
		}
}
