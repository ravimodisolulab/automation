/**
 * 
 */
package Application_Module;


import org.testng.Reporter;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;

/**
 * @author Dell
 *
 */
public class MyInvites extends Actions_Class
{
	public static String classname = null;
	public static String meetingTime;
	public static void receivedInvitaionList() throws Exception
	{
		Thread.sleep(2000);
		clickElement("xpath", miReceivedTab);
		classname = classname + "Received Tab open";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(2000);
	}
	
	public static void sentInvitationList() throws Exception
	{
		clickElement("xpath", miSentTab);
		
		classname = classname + "Received Tab open";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(2000);
	}
	
	public static void declineInvitationFromReceivedByMentor() throws Exception
	{
		
		receivedInvitaionList();
		Thread.sleep(8000);
		meetingTime = getText("xpath", miReceiveTabMeetingTime); Thread.sleep(2000);
		classname = classname + meetingTime;
	
		clickElement("xpath", miDeclineNormalFromReceive);
		Thread.sleep(5000);
		clickElement("xpath", miDeclineNormalFromReceivePopupOk);
		Thread.sleep(5000);
		classname = classname + "Mentor decline normal invite.";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
		
		Reporter.log("Mentor Decline user's invitation from receive tab..." + classname);
	}
	
	public static void declineInvitationFromReceivedByUser() throws Exception
	{
		
		receivedInvitaionList();
		Thread.sleep(8000);
				
		meetingTime = getText("xpath", miReceiveTabMeetingTime); Thread.sleep(2000);
		classname = classname + meetingTime;
		
		clickElement("xpath", miDeclinePrivateFromReceive);
		Thread.sleep(2000);
		clickElement("xpath", miDeclinePrivateFromReceivePopupOk);
		Thread.sleep(5000);
		classname = classname + meetingTime +"User decline Private invite.";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
		
		Reporter.log("User Decline Mentor's invitation from receive tab..." + classname);
	}
	
	public static void acceptInvitationFromReceivedByMentor() throws Exception
	{
		receivedInvitaionList();
		Thread.sleep(8000);
		meetingTime = getAtribute("xpath", miReceiveTabMeetingTime, "Type"); Thread.sleep(2000);
		classname = classname + "Mentor or User Accepted Invitation From Received - " + meetingTime;
		System.out.println(meetingTime);
		meetingTime = getText("xpath", miReceiveTabMeetingTime); Thread.sleep(2000);
		classname = classname + meetingTime;
		
		
		clickElement("xpath", miReceiveTabMeetingTime);
		Thread.sleep(5000);
		clickElement("xpath", miNormalAcceptFromReceive);
		Thread.sleep(5000);
		
		classname = classname + "Normal Invite Meeting Accepted by Mentor";
		Success_FailureMessageSheet.storeMessageinfile(classname); Thread.sleep(5000);
		Reporter.log("Mentor Accept user's invitation from receive tab..." + classname);
	}
	
	public static void acceptInvitationFromReceivedByUser() throws Exception
	{
		receivedInvitaionList();
		Thread.sleep(8000);
		meetingTime = getAtribute("xpath", miReceiveTabMeetingTime, "Type"); Thread.sleep(2000);
		classname = classname + "Mentor or User Accepted Invitation From Received - " + meetingTime;
		meetingTime = getText("xpath", miReceiveTabMeetingTime); Thread.sleep(2000);
		classname = classname + meetingTime;
		
		clickElement("xpath", miReceiveTabMeetingTime);
		Thread.sleep(5000);
		clickElement("xpath", miPrivateAcceptFromReceive);
		Thread.sleep(5000);
		
		classname = classname + "Private Invite Meeting Accepted by User";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
		Reporter.log("User Accept Mentor's invitation from receive tab..." + classname);
	}
	
	public static void declineInvitationFromSentListByUser() throws Exception
	{
		sentInvitationList();
		Thread.sleep(8000);
	
		meetingTime = getText("xpath", miSentTabMeetingTime); Thread.sleep(2000);
		classname = classname + meetingTime;
		Thread.sleep(5000);
		
		clickElement("xpath", miCancellNoramlFromSent);
		classname = classname + "User decline Normal invite before accept it by Mentor.";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		
		Reporter.log("User Cancell ivitation from Sent tab before accept it by Mentor..." + classname);
	}
	
	public static void declineInvitationFromSentListByMentor() throws Exception
	{
		sentInvitationList();
		Thread.sleep(8000);
	
		meetingTime = getText("xpath", miSentTabMeetingTime); Thread.sleep(2000);
		classname = classname + meetingTime;
		Thread.sleep(5000);		
		clickElement("xpath", miCancellPrivateFromSent);
		
		classname = classname + "Mentor decline private invite before accept it by User";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		
		Reporter.log("Mentor Cancell ivitation from Sent tab before accept it by User..." + classname);
	}
}
// Implicit Wait
//WebDriverWait wait = new WebDriverWait(driver,30);
//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(miCancellNoramlFromSent)));
