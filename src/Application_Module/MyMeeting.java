package Application_Module;

import static org.testng.Assert.assertEquals;

import org.testng.Reporter;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.comman_API;

public final class MyMeeting extends Actions_Class
{
	public static String MeetingId;
	public static String MeetingStatus;
	public static String classname = "MyMeeting";
	public static String PopupText;
	public static String ReasonText;
	
	public static String futureTab() throws Exception
	{
		clickElement("xpath", mmFutureTab);
		Thread.sleep(2000);
		classname = classname + "future meeting tab ";
		return classname;
	}
	
	public static String completedTab() throws Exception
	{
		clickElement("xpath", mmCompletedTab);
		Thread.sleep(2000);
		classname = classname + "completed meeting tab ";
		return classname;
	}
	
	
	
	public static void cancell_Meeting_ByUser() throws Exception
	{
		classname = futureTab();
		Thread.sleep(5000);
		MeetingId = getText("xpath", mmMeetingID);
		Thread.sleep(2000);
		MeetingStatus = getText("xpath", mmMeetingStatusAtFuture);
		Thread.sleep(2000);
		clickElement("xpath", mmCancellMeetingByUSer);
		Thread.sleep(2000);
		PopupText = getText("xpath", mmCancelMeetingPopupUserText);
		Thread.sleep(2000);
		clickElement("xpath", mmCancelMeetingPopupUserOK);
		Thread.sleep(2000);
		ReasonText = getText("xpath", mmCancelMeetingResonPopupUserText);
		Thread.sleep(2000);
		clickElement("xpath", mmCancelMeetingResonPopupUserOk);
		Thread.sleep(10000);
		
		classname = classname + "cancell_Meeting_By User - Meeting Cancell By User from My Meeting List" + MeetingId + MeetingStatus + PopupText + ReasonText;
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
		
		Reporter.log("User Cancell Accepted Meeting..." + classname);
	}
	
	public static void cancell_Meeting_ByMentor() throws Exception
	{
		classname = futureTab();
		Thread.sleep(5000);
		MeetingId = getText("xpath", mmMeetingID);
		Thread.sleep(2000);
		MeetingStatus = getText("xpath", mmMeetingStatusAtFuture);
		Thread.sleep(2000);
		clickElement("xpath", mmCancellMeetingByMentor);
		Thread.sleep(2000);
		PopupText = getText("xpath", mmCancelMeetingPopupMentorText);
		Thread.sleep(2000);
		clickElement("xpath", mmCancelMeetingPopupMentorOK);
		Thread.sleep(2000);
		ReasonText = getText("xpath", mmCancelMeetingResonPopupMentorText);
		Thread.sleep(2000);
		clickElement("xpath", mmCancelMeetingResonPopupMentorOK);
		Thread.sleep(10000);
		
		classname = classname + "cancell_Meeting_By Mentor - Meeting Cancell By Mentor from My Meeting List" + MeetingId + MeetingStatus + PopupText + ReasonText;
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
		
		Reporter.log("Mentor Cancell Accepted Meeting..." + classname);
	}
	
	public static void API_CALL(String urlname) throws Exception
	{
		Thread.sleep(5000);
		String Result =  comman_API.callApi(urlname);
		Thread.sleep(5000);
		Reporter.log(Result);
		assertEquals(Result, "402");
		Reporter.log("Meeting Status change successfully");
	}
	
}
