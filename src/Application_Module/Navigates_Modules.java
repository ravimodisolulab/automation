package Application_Module;

import Config.Actions_Class;

public class Navigates_Modules extends Actions_Class
{
	
	// Open Website
	public static void openwebsite() throws Exception
	{
		driver.get(url);
	}
	
	// Redirect to Login Page
	public static void RedirectToLogin() throws Exception
	{
		clickElement("xpath", clickOnLogin);
	}
	
	public static void RedirectToMentorLogout() throws Exception
	{
		clickElement("xpath", clickOnMenu);
		Thread.sleep(1000);
		clickElement("xpath", mentorLogout);
		Thread.sleep(10000);
	}

	public static void RedirectToUserLogout() throws Exception 
	{
		// TODO Auto-generated method stub
		clickElement("xpath", clickOnMenu);
		Thread.sleep(1000);
		clickElement("xpath", userLogout);
	}

	
	
}
