package Application_Module;

import static org.testng.Assert.assertEquals;

import java.sql.Connection;
import java.sql.ResultSet;
import org.testng.Reporter;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.TestData_Sheet;
import Config.database_Conncetion;

public class paymentVarificationWithDatabase extends Actions_Class
{
	public static String query;
	public static String UserDBValue;
	public static String MentorDBValue;
	public static String WissenxDBValue;
	public static String TotalDBValue;
	public static String UserEmailId;
	public static String UserEXcelValue;
	public static String MentorExcelValue;
	public static String WissenxExcelValue;
	public static String TotalExcelValue;
	public static String className = "Payment Varification with data base.";
	public static String message;
	public static boolean result;
	public static String line = "-----------------------------------";
	public static ResultSet rs;
	public static Connection connection = null;
	
	
	public static void bookNormalMeeting_ByUser(String xlsxpath) throws Exception
	{	
		
		Thread.sleep(2000);
		DataSheetFile(xlsxpath, "Login");
		emailaddress = TestData_Sheet.getCellData(2, 0);
		MentorExcelValue = TestData_Sheet.getCellData(5, 5);	
		WissenxExcelValue = TestData_Sheet.getCellData(5, 6);
		TotalExcelValue = TestData_Sheet.getCellData(5, 7);
		
	    query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.final_user_paid_amount,2) final_user_paid_amount, refund_paid_to_user FROM users urs" +
				" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.requester_id " +
				" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = '" + emailaddress + "' AND mi.invitation_type = '0' AND mi.status = 'pending' ORDER BY ins.invitation_id DESC LIMIT 1 ";
	    
	   
	    rs = (ResultSet) database_Conncetion.dbconnection(query);
		while(rs.next())  
		{			WissenxDBValue = rs.getString(2);
					MentorDBValue = rs.getString(3);
					UserDBValue = rs.getString(4);
					System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
					
							
					Thread.sleep(5000); 
					
					result = excelAndDatabaseValueBookMeeting(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, TotalExcelValue, UserDBValue);
					assertEquals(result, true);
						
		}
		System.out.println("Call Finally - to close connection");
		//rs.close();
	
		
	
	}
	
	public static void beforeAcceptNormalInvite_CancellByUser_CheckPayment(String xlxspath) throws Exception
	{	
		Thread.sleep(2000);
		DataSheetFile(xlxspath, "Login");
		emailaddress = TestData_Sheet.getCellData(2, 0);
		UserEXcelValue = TestData_Sheet.getCellData(3, 17);	
		WissenxExcelValue = TestData_Sheet.getCellData(3, 18);
		MentorExcelValue = TestData_Sheet.getCellData(3, 19);
		
		query = " SELECT urs.user_id,ROUND(ins.final_user_paid_amount,2) final_user_paid_amount, ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
				" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.requester_id " +
				" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = ' "  + emailaddress +  " ' AND mi.invitation_type = '0' AND mi.status = 'cancelled' ORDER BY ins.invitation_id DESC LIMIT 1 ";
		
		Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						UserDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						
						Thread.sleep(5000);
						result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
						assertEquals(result, true);
			}
		//rs.close();
		
	}
	
	public static void beforeAcceptNormalInvite_CancellByMentor_CheckPayment(String xlxspath) throws Exception
	{	
		Thread.sleep(2000);
		DataSheetFile(xlxspath, "Login");
		emailaddress = TestData_Sheet.getCellData(2, 0);
		UserEXcelValue = TestData_Sheet.getCellData(3, 17);	
		WissenxExcelValue = TestData_Sheet.getCellData(3, 18);
		MentorExcelValue = TestData_Sheet.getCellData(3, 19);
		
		query=   " SELECT urs.user_id,ROUND(ins.final_user_paid_amount,2) final_user_paid_amount, ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
				" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.requester_id " +
				" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = ' "  + emailaddress +  " ' AND mi.invitation_type = '0' AND mi.status = 'declined' ORDER BY ins.invitation_id DESC LIMIT 1 ";
		
		
		Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						UserDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						
						Thread.sleep(5000);
						result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
						assertEquals(result, true);
			}
		
		rs.close();
	}
	
	public static void AcceptNormalInvite_ByMentor_CheckPayment(String xlxspath) throws Exception
	{	
		Thread.sleep(2000);
		DataSheetFile(xlxspath, "Login");
		emailaddress = TestData_Sheet.getCellData(2, 0);
		UserEXcelValue = TestData_Sheet.getCellData(3, 8);	
		WissenxExcelValue = TestData_Sheet.getCellData(3, 9);
		MentorExcelValue = TestData_Sheet.getCellData(3, 10);
		
		 query =    " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.final_user_paid_amount,2) final_user_paid_amount, refund_paid_to_user FROM users urs " +				
					" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.requester_id " +
					" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
					" WHERE urs.email = '" + emailaddress + "' AND mi.invitation_type = '0' AND mi.status = 'accepted' ORDER BY ins.invitation_id DESC LIMIT 1 ";
		    
		
		Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						UserDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						
						Thread.sleep(5000);
						result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
						assertEquals(result, true);
			}
		
		
	}
	
	
	public static void afterAcceptNormalInvite_CancellByUser_CheckPayment(String xlxspath) throws Exception
	{
		Thread.sleep(2000);
		DataSheetFile(xlxspath, "Login");
		emailaddress = TestData_Sheet.getCellData(2, 0);
		UserEXcelValue = TestData_Sheet.getCellData(3, 11);	
		WissenxExcelValue = TestData_Sheet.getCellData(3, 12);
		MentorExcelValue = TestData_Sheet.getCellData(3, 13);
		
		query = " SELECT urs.user_id,ROUND(ins.final_user_paid_amount,2) final_user_paid_amount, ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
				" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.requester_id " +
				" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = '" + emailaddress + "' AND mi.invitation_type = '0' AND mi.status = 'acceptedmeetingcancel' ORDER BY ins.invitation_id DESC LIMIT 1 ";
	    
		
		Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						UserDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						
						Thread.sleep(5000);
						result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
						assertEquals(result, true);
			}
		
	}
	
	public static void afterAcceptNormalInvite_CancellByMentor_CheckPayment(String xlxspath) throws Exception
	{
		Thread.sleep(2000);
		DataSheetFile(xlxspath, "Login");
		emailaddress = TestData_Sheet.getCellData(2, 0);
		UserEXcelValue = TestData_Sheet.getCellData(3, 14);	
		WissenxExcelValue = TestData_Sheet.getCellData(3, 15);
		MentorExcelValue = TestData_Sheet.getCellData(3, 16);
		
		query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
				" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.requester_id " +
				" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = '" + emailaddress + "' AND mi.invitation_type = '0' AND mi.status = 'acceptedmeetingcancel' ORDER BY ins.invitation_id DESC LIMIT 1 ";
	    
		
		Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						UserDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						
						Thread.sleep(5000);
						result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
						assertEquals(result, true);
			}
			
	
	}
	
	
	
	// Private Invite flow
	
	/**public static void beforeAcceptPrivateInvite_CancellByMentor_CheckPayment(String xlxspath) throws Exception
	{	
		Thread.sleep(2000);
		DataSheetFile(xlsxpath, "Login");
		emailaddress = TestData_Sheet.getCellData(3, 0);
		UserEXcelValue = TestData_Sheet.getCellData(3, 11);	
		WissenxExcelValue = TestData_Sheet.getCellData(3, 12);
		MentorExcelValue = TestData_Sheet.getCellData(3, 13);
		
		query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.final_user_paid_amount,2) final_user_paid_amount FROM users urs " +
				" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
				" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'cancelled' ORDER BY ins.invitation_id DESC LIMIT 1 ";
		
		
		Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						TotalDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						
						Thread.sleep(5000);
						if((WissenxExcelValue.equals(WissenxDBValue)) && (MentorExcelValue.equals(MentorDBValue)) && (TotalExcelValue.equals(TotalDBValue)))
						{
							className = className +"After done payment - calculation match perfactly with database..." +
										"Wissenx charges:--" + WissenxDBValue +" , " + WissenxExcelValue + 
									 	"Mentor Charges:--" + MentorDBValue + " , " + MentorExcelValue +
									 	"Total Charges:-- " + TotalDBValue + " , " + TotalExcelValue;
							Success_FailureMessageSheet.storeMessageinfile(className);
							System.out.println("Payment Varified Successfully....");
						}
						else
						{
									className = className +"After done payment - calculation Does not match perfactly with database..." +
									"Wissenx charges:--" + WissenxDBValue +" , " + WissenxExcelValue + 
								 	"Mentor Charges:--" + MentorDBValue + " , " + MentorExcelValue +
								 	"Total Charges:-- " + TotalDBValue + " , " + TotalExcelValue;
							Success_FailureMessageSheet.storeMessageinfile(className);
							System.out.println("Payment Varified Successfully....");
						}
			}

		
	}
	
	public static void beforeAcceptPrivateInvite_CancellByUser_CheckPayment(String xlxspath) throws Exception
	{	
		Thread.sleep(2000);
		DataSheetFile(xlsxpath, "Login");
		emailaddress = TestData_Sheet.getCellData(3, 0);
		UserEXcelValue = TestData_Sheet.getCellData(3, 11);	
		WissenxExcelValue = TestData_Sheet.getCellData(3, 12);
		MentorExcelValue = TestData_Sheet.getCellData(3, 13);
		
		query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.final_user_paid_amount,2) final_user_paid_amount FROM users urs " +
				" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
				" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'declined' ORDER BY ins.invitation_id DESC LIMIT 1 ";
		
		Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						TotalDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						
						Thread.sleep(5000);
						if((WissenxExcelValue.equals(WissenxDBValue)) && (MentorExcelValue.equals(MentorDBValue)) && (TotalExcelValue.equals(TotalDBValue)))
						{
							className = className +"After done payment - calculation match perfactly with database..." +
										"Wissenx charges:--" + WissenxDBValue +" , " + WissenxExcelValue + 
									 	"Mentor Charges:--" + MentorDBValue + " , " + MentorExcelValue +
									 	"Total Charges:-- " + TotalDBValue + " , " + TotalExcelValue;
							Success_FailureMessageSheet.storeMessageinfile(className);
							System.out.println("Payment Varified Successfully....");
						}
						else
						{
									className = className +"After done payment - calculation Does not match perfactly with database..." +
									"Wissenx charges:--" + WissenxDBValue +" , " + WissenxExcelValue + 
								 	"Mentor Charges:--" + MentorDBValue + " , " + MentorExcelValue +
								 	"Total Charges:-- " + TotalDBValue + " , " + TotalExcelValue;
							Success_FailureMessageSheet.storeMessageinfile(className);
							System.out.println("Payment Varified Successfully....");
						}
			}

		
	}*/
	
	public static void afterAcceptPrivateInvite_ByUser(String xlsxpath) throws Exception
	{
		Thread.sleep(2000);
		DataSheetFile(xlsxpath, "Login");
		emailaddress = TestData_Sheet.getCellData(2, 0);
		MentorExcelValue = TestData_Sheet.getCellData(5, 5);	
		WissenxExcelValue = TestData_Sheet.getCellData(5, 6);
		TotalExcelValue = TestData_Sheet.getCellData(5, 7);
		
		query = "  SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.final_user_paid_amount,2) final_user_paid_amount, refund_paid_to_user " + 
				" FROM users urs LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = '" + emailaddress + "' AND mi.invitation_type = '1' AND mi.status = 'accepted' ORDER BY ins.invitation_id DESC LIMIT 1";
		
		 Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						TotalDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						
					Thread.sleep(5000);
					result = excelAndDatabaseValueBookMeeting(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, TotalExcelValue, TotalDBValue);
					assertEquals(result, true);
			}
	}
	
	public static void afterAcceptPrivateInvite_CancellByUser_CheckPayment(String xlxspath) throws Exception
	{
		Thread.sleep(2000);
		DataSheetFile(xlxspath, "Login");
		emailaddress = TestData_Sheet.getCellData(2, 0);
		UserEXcelValue = TestData_Sheet.getCellData(3, 11);	
		WissenxExcelValue = TestData_Sheet.getCellData(3, 12);
		MentorExcelValue = TestData_Sheet.getCellData(3, 13);
		
		query = " SELECT urs.user_id,ROUND(ins.final_user_paid_amount,2) final_user_paid_amount, ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
				" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
				" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'acceptedmeetingcancel' ORDER BY ins.invitation_id DESC LIMIT 1 ";
		
		
		Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						UserDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						
						Thread.sleep(5000);
						result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
						assertEquals(result, true);
			}
	}
	
	public static void afterAcceptPrivateInvite_CancellByMentor_CheckPayment(String xlxspath) throws Exception
	{
		Thread.sleep(2000);
		DataSheetFile(xlxspath, "Login");
		emailaddress = TestData_Sheet.getCellData(2, 0);
		UserEXcelValue = TestData_Sheet.getCellData(3, 14);	
		WissenxExcelValue = TestData_Sheet.getCellData(3, 15);
		MentorExcelValue = TestData_Sheet.getCellData(3, 16);
		
		
		query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
				" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
				" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
				" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'acceptedmeetingcancel' ORDER BY ins.invitation_id DESC LIMIT 1 ";
		
		Thread.sleep(5000);
		 rs = (ResultSet) database_Conncetion.dbconnection(query);
			while(rs.next())  
			{			WissenxDBValue = rs.getString(2);
						MentorDBValue = rs.getString(3);
						UserDBValue = rs.getString(4);
						System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
						Thread.sleep(5000);
						
						result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
						assertEquals(result, true);
						
			}
		}
	
	
	
	
	//------------------------------------------
	

	/** Match calculation */
	public static boolean excelAndDatabaseValue
	(
			String WissenxExcelVaule, String WissenxDBValue,String MentorExcelValue, String MentorDBValue,String UserEXcelValue, String UserDBValue
	) throws Exception
	{
			if((WissenxExcelVaule.equals(WissenxDBValue)) && (MentorExcelValue.equals(MentorDBValue)) && (UserEXcelValue.equals(UserDBValue)))
			{

				className = "After done payment - calculation match perfactly with database..." +"\n" +
						"Wissenx charges:--" + "Database value:-- " + WissenxDBValue +" , " + " Excel Value:--"  + WissenxExcelVaule + 
					 	"Mentor Charges:--"  + "Database value:-- " + MentorDBValue + " , " + " Excel Value:--"  + MentorExcelValue +
					 	"User Charges:-- "  + "Database value:-- " + UserDBValue + " , "  + " Excel Value:--"  + UserEXcelValue;
						Thread.sleep(5000); 
						Success_FailureMessageSheet.storeMessageinfile(line);
						Success_FailureMessageSheet.storeMessageinfile(className);
						System.out.println("Payment Varified Successfully....");
						Reporter.log(className);
				return true;
			}
			else
			{

				className = "After done payment - calculation does not match perfactly with database..." +"\n" +
						"Wissenx charges:--" + "Database value:-- " + WissenxDBValue +" , " + " Excel Value:--"  + WissenxExcelVaule + 
					 	"Mentor Charges:--"  + "Database value:-- " + MentorDBValue + " , " + " Excel Value:--"  + MentorExcelValue +
					 	"User Charges:-- "  + "Database value:-- " + UserDBValue + " , "  + " Excel Value:--"  + UserEXcelValue;
						Thread.sleep(5000); 
						Success_FailureMessageSheet.storeMessageinfile(className);
						System.out.println("Payment Varified Successfully....");
						Reporter.log(className);
				return false;		
			}
			
	
	}
	
	public static boolean excelAndDatabaseValueBookMeeting
	(
			String WissenxExcelVaule, String WissenxDBValue,String MentorExcelValue, String MentorDBValue,String UserEXcelValue, String UserDBValue
	) throws Exception
	{
			if((WissenxExcelVaule.equals(WissenxDBValue)) && (MentorExcelValue.equals(MentorDBValue)) && (UserEXcelValue.equals(UserDBValue)))
			{
			
					className = "After done payment - calculation match perfactly with database..." +
					"Wissenx charges:--" + "Database value:-- " + WissenxDBValue +" , " + " Excel Value:--"  + WissenxExcelVaule + 
					"Mentor Charges:--"  + "Database value:-- " + MentorDBValue + " , " + " Excel Value:--"  + MentorExcelValue +
					"Total Charges:-- "  + "Database value:-- " + UserDBValue + " , "  + " Excel Value:--"  + UserEXcelValue;
					Thread.sleep(5000); 
					Success_FailureMessageSheet.storeMessageinfile(line);
					Success_FailureMessageSheet.storeMessageinfile(className);
					System.out.println("Payment Varified Successfully....");
					Reporter.log(className);
				return true;
			}
			else
			{
			
					className = "After done payment - calculation does not match perfactly with database..." +
							"Wissenx charges:--" + "Database value:-- " + WissenxDBValue +" , " + " Excel Value:--"  + WissenxExcelVaule + 
							"Mentor Charges:--"  + "Database value:-- " + MentorDBValue + " , " + " Excel Value:--"  + MentorExcelValue +
							"Total Charges:-- "  + "Database value:-- " + UserDBValue + " , "  + " Excel Value:--"  + UserEXcelValue;
					Thread.sleep(5000); 
					Success_FailureMessageSheet.storeMessageinfile(className);
					System.out.println("Payment Varified Successfully....");
					Reporter.log(className);
				return false;		
			}


	}
	

	/**
	 * After accept meeting by mentor/user below method will be in use
	 */
		public static void afterCompleteMeetingSuccessfully_CheckPayment(String xlxspath) throws Exception
		{
			Thread.sleep(2000);
			DataSheetFile(xlxspath, "Login");
			emailaddress = TestData_Sheet.getCellData(2, 0);
			UserEXcelValue = TestData_Sheet.getCellData(9, 1);	
			WissenxExcelValue = TestData_Sheet.getCellData(9, 2);
			MentorExcelValue = TestData_Sheet.getCellData(9, 3);
			
			
			query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
					" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
					" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
					" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'completed' ORDER BY ins.invitation_id DESC LIMIT 1 ";
			
			Thread.sleep(5000);
			 rs = (ResultSet) database_Conncetion.dbconnection(query);
				while(rs.next())  
				{			WissenxDBValue = rs.getString(2);
							MentorDBValue = rs.getString(3);
							UserDBValue = rs.getString(4);
							System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
							Thread.sleep(5000);
							
							result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
							assertEquals(result, true);
							
				}
		}
		
		public static void afterTimeOut_CheckPayment(String xlxspath) throws Exception
		{
			Thread.sleep(2000);
			DataSheetFile(xlxspath, "Login");
			emailaddress = TestData_Sheet.getCellData(2, 0);
			UserEXcelValue = TestData_Sheet.getCellData(9, 16);	
			WissenxExcelValue = TestData_Sheet.getCellData(9, 17);
			MentorExcelValue = TestData_Sheet.getCellData(9, 18);
			
			
			query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
					" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
					" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
					" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'timedout' ORDER BY ins.invitation_id DESC LIMIT 1 ";
			
			Thread.sleep(5000);
			 rs = (ResultSet) database_Conncetion.dbconnection(query);
				while(rs.next())  
				{			WissenxDBValue = rs.getString(2);
							MentorDBValue = rs.getString(3);
							UserDBValue = rs.getString(4);
							System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
							Thread.sleep(5000);
							
							result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
							assertEquals(result, true);
							
				}
				
		}
		
		public static void afterNoShowByUser_CheckPayment(String xlxspath) throws Exception
		{
			Thread.sleep(2000);
			DataSheetFile(xlxspath, "Login");
			emailaddress = TestData_Sheet.getCellData(2, 0);
			UserEXcelValue = TestData_Sheet.getCellData(9, 4);	
			WissenxExcelValue = TestData_Sheet.getCellData(9, 5);
			MentorExcelValue = TestData_Sheet.getCellData(9, 6);
			
			
			query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
					" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
					" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
					" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'no_show_user' ORDER BY ins.invitation_id DESC LIMIT 1 ";
			
			Thread.sleep(5000);
			 rs = (ResultSet) database_Conncetion.dbconnection(query);
				while(rs.next())  
				{			WissenxDBValue = rs.getString(2);
							MentorDBValue = rs.getString(3);
							UserDBValue = rs.getString(5);
							System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
							Thread.sleep(5000);
							
							result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
							assertEquals(result, true);
							
				}
		}
		
		public static void afterNoShowByMentor_CheckPayment(String xlxspath) throws Exception
		{
			Thread.sleep(2000);
			DataSheetFile(xlxspath, "Login");
			emailaddress = TestData_Sheet.getCellData(2, 0);
			UserEXcelValue = TestData_Sheet.getCellData(9, 7);	
			WissenxExcelValue = TestData_Sheet.getCellData(9, 8);
			MentorExcelValue = TestData_Sheet.getCellData(9, 9);
			
			
			query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
					" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
					" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
					" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'no_show_mentor' ORDER BY ins.invitation_id DESC LIMIT 1 ";
			
			Thread.sleep(5000);
			 rs = (ResultSet) database_Conncetion.dbconnection(query);
				while(rs.next())  
				{			WissenxDBValue = rs.getString(2);
							MentorDBValue = rs.getString(3);
							UserDBValue = rs.getString(4);
							System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
							Thread.sleep(5000);
							
							result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
							assertEquals(result, true);
							
				}
		}
	
		public static void afterNoShowByBoth_CheckPayment(String xlxspath) throws Exception
		{
			Thread.sleep(2000);
			DataSheetFile(xlxspath, "Login");
			emailaddress = TestData_Sheet.getCellData(2, 0);
			UserEXcelValue = TestData_Sheet.getCellData(9, 10);	
			WissenxExcelValue = TestData_Sheet.getCellData(9, 11);
			MentorExcelValue = TestData_Sheet.getCellData(9, 12);
			
			
			query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
					" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
					" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
					" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'no_show_both' ORDER BY ins.invitation_id DESC LIMIT 1 ";
			
			Thread.sleep(5000);
			 rs = (ResultSet) database_Conncetion.dbconnection(query);
				while(rs.next())  
				{			WissenxDBValue = rs.getString(2);
							MentorDBValue = rs.getString(3);
							UserDBValue = rs.getString(4);
							System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
							Thread.sleep(5000);
							
							result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
							assertEquals(result, true);
							
				}
		}
	
		public static void afterFullRefundByAdmin_CheckPayment(String xlxspath) throws Exception
		{
			Thread.sleep(2000);
			DataSheetFile(xlxspath, "Login");
			emailaddress = TestData_Sheet.getCellData(2, 0);
			UserEXcelValue = TestData_Sheet.getCellData(9, 13);	
			WissenxExcelValue = TestData_Sheet.getCellData(9, 14);
			MentorExcelValue = TestData_Sheet.getCellData(9, 15);
			
			
			query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
					" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
					" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
					" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'fullrefund' ORDER BY ins.invitation_id DESC LIMIT 1 ";
			
			Thread.sleep(5000);
			 rs = (ResultSet) database_Conncetion.dbconnection(query);
				while(rs.next())  
				{			WissenxDBValue = rs.getString(2);
							MentorDBValue = rs.getString(3);
							UserDBValue = rs.getString(4);
							System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
							Thread.sleep(5000);
							
							result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
							assertEquals(result, true);
							
				}
		}
	
		public static void afterPartialRefundByAdmin_CheckPayment(String xlxspath) throws Exception
		{
			Thread.sleep(2000);
			DataSheetFile(xlxspath, "Login");
			emailaddress = TestData_Sheet.getCellData(2, 0);
			UserEXcelValue = TestData_Sheet.getCellData(9, 16);	
			WissenxExcelValue = TestData_Sheet.getCellData(9, 17);
			MentorExcelValue = TestData_Sheet.getCellData(9, 18);
			
			
			query = " SELECT urs.user_id,ROUND(ins.final_pay_to_wissenx,2) final_pay_to_wissenx,ROUND(ins.final_pay_to_mentor,2) final_pay_to_mentor,ROUND(ins.refund_paid_to_user,2) refund_paid_to_user FROM users urs " +				
					" LEFT JOIN meeting_invitations mi ON urs.user_id = mi.user_id " +
					" LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
					" WHERE urs.email = '" + emailaddress + "'  AND mi.invitation_type = '1' AND mi.status = 'partialrefund' ORDER BY ins.invitation_id DESC LIMIT 1 ";
			
			Thread.sleep(5000);
			 rs = (ResultSet) database_Conncetion.dbconnection(query);
				while(rs.next())  
				{			WissenxDBValue = rs.getString(2);
							MentorDBValue = rs.getString(3);
							UserDBValue = rs.getString(4);
							System.out.print("wissenx chg ::" +rs.getString(2) +", mentor chg ::"+ rs.getString(3) +", total amount ::"+ rs.getString(4));
							Thread.sleep(5000);
							
							result = excelAndDatabaseValue(WissenxExcelValue, WissenxDBValue, MentorExcelValue, MentorDBValue, UserEXcelValue, UserDBValue);
							assertEquals(result, true);
							
				}
		}

}



