package Application_Module;

import org.testng.Reporter;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;

public class paymentVarificationWithDashboard extends Actions_Class
{

	public static String className = null;
	public static String ExcelValue = null;
	public static String DashboardValue = null;
	
	
	public static void afterCompleteMeeting_CheckWithUser(String xlxspath) throws Exception
	{
		DataSheetFile(xlxspath, "Login");
	}
	
	public static void afterCompleteMeeting_CheckWithMentor(String xlxspath)
	{
		
	}
	
	public static void afterTimeOutMeeting_CheckWithUser(String xlxspath)
	{
		
	}
	
	public static void afterTimeOutMeeting_CheckWithMentor(String xlxspath)
	{
		
	}
	
	public static void afterNoShowByUserMeeting_CheckWithUser(String xlxspath)
	{
		
	}
	
	public static void afterNoShowByUserMeeting_CheckWithMentor(String xlxspath)
	{
		
	}
	
	public static void afterNoShowByMentorMeeting_CheckWithUser(String xlxspath)
	{
		
	}
	
	public static void afterNoShowByMentorMeeting_CheckWithMentor(String xlxspath)
	{
		
	}
	
	public static void afterNoShowByBothrMeeting_CheckWithUser(String xlxspath)
	{
		
	}
	
	public static void afterNoShowByBothMeeting_CheckWithMentor(String xlxspath)
	{
		
	}
	
	public static void afterFullRefundByAdmin_CheckWithUser(String xlxspath)
	{
		
	}
	
	public static void afterFullRefundByAdmin_CheckWithMentor(String xlxspath)
	{
		
	}
	
	public static void afterPartialRefundByAdmin_CheckWithUser(String xlxspath)
	{
		
	}
	
	public static void afterPartialRefundByAdmin_CheckWithMentor(String xlxspath)
	{
		
	}
	
	
	public static boolean excelAndDashboard_ValueMatch
	(
			String ExcelValue, String DashBoardValue
	) throws Exception
	{
			if(ExcelValue.equals(DashBoardValue))
			{
			
					className = "After done payment - calculation match perfactly with database..." +"Excel Value" + ExcelValue + " Dashboard Value " + DashBoardValue;
					Success_FailureMessageSheet.storeMessageinfile(className);
					System.out.println("Payment Varified Successfully with Dashboard....");
					Reporter.log(className);
					return true;
			}
			else
			{
			
				className = "After done payment - calculation match perfactly with database..." +"Excel Value" + ExcelValue + " Dashboard Value " + DashBoardValue;
				Success_FailureMessageSheet.storeMessageinfile(className);
				System.out.println("Payment Varified Successfully with Dashboard....");
				Reporter.log(className);
				return false;		
			}


	}
}
