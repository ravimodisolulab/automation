package Application_Module;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;

public class MyAccountMenu extends Actions_Class
{	
	public static String classname = "MyAccountMenu";
	public static String openMyAccount() throws Exception
	{
		clickElement("xpath", maMenu);
		Thread.sleep(2000);
		
		classname = classname + "Open My Account Menu";
		return classname;
	}
	
	public static void openDashboard() throws Exception
	{
		classname = openMyAccount();
		clickElement("xpath", maMenuMentorDashboard);
		
		classname = classname + "Open Dashboard Page";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
	}
	public static void openMentorCalendar() throws Exception
	{
		classname = openMyAccount();
		clickElement("xpath", maMenuMentorCalendar);
		Thread.sleep(5000);
		
		classname = classname + "Open Mentor Calendar Page";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
	}
	
		
	public static void openMentorMyInvite() throws Exception
	{
		classname = openMyAccount();
		clickElement("xpath", maMenuMentorMyInvite);
		Thread.sleep(5000);
		
		classname = classname + "Open Mentor My Invite Page";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
	}
	
	public static void openUserMyInvite() throws Exception
	{
		classname = openMyAccount();
		clickElement("xpath", maMenuUserMyInvite);
		Thread.sleep(5000);
		
		classname = classname + "Open Mentor User My invite Page";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
	}
	
	public static void openMentorMyMeetings() throws Exception
	{
		classname = openMyAccount();
		clickElement("xpath", maMenuMentorMyMeeting);
		Thread.sleep(5000);
		
		classname = classname + "Open Mentor My Meeting Page";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
	}
	
	public static void openUserMyMeeting() throws Exception
	{
		classname = openMyAccount();
		clickElement("xpath", maMenuUserMyMeeting);
		Thread.sleep(5000);
		
		classname = classname + "Open User My Meeting Page";
		Success_FailureMessageSheet.storeMessageinfile(classname);
		Thread.sleep(5000);
	}
	
}
 