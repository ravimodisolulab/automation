/**
 * 
 */
package Application_Module;

import static org.testng.Assert.assertEquals;
import org.testng.Reporter;
import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.TestData_Sheet;

/**
 * @author welcome
 *
 */
public class Paypal_Payment extends Actions_Class
{
	public static String classname;
	public static String MentorCharge;
	public static String WissenxCharge;
	public static String TotalCharge;
	public static boolean result;
	
	public static void checkMentorAmount(String xlxspath) throws Exception
	{
		classname = "Paypal_Payment - checkMentorAmount";
		TestData_Sheet.setExcelFile(xlxspath, "Login");
		MentorRate = TestData_Sheet.getCellData(3, 5);
		System.out.println(MentorRate);
		WissenxCharge = TestData_Sheet.getCellData(3, 6);
		System.out.println(WissensCharge);
		TotalCharge = TestData_Sheet.getCellData(3, 7);
		System.out.println(TotalCharge);
		
			
		String mentor = getText("xpath", pmMentorRate);
		String wissenx = getText("xpath", pmWissenxCharge);
		String total = getText("xpath", pmTotalPayment);
		
		result = paymentVarificationWithDatabase.excelAndDatabaseValue(WissenxCharge, wissenx, MentorRate, mentor, TotalCharge, total);
		assertEquals(result, true);
	}
	public static void creditCardNumber_Month_Year_CVVNumber(String xlxspath) throws Exception
	{
		classname =  "Paypal_Payment - creditCardNumber_Month_Year_CVVNumber";
		TestData_Sheet.setExcelFile(xlxspath, "PaymentPart");
		creditCardNumber =  TestData_Sheet.getCellData(1,0);
		System.out.println(creditCardNumber);
		int mon =  TestData_Sheet.getCellDataInt(1, 1);
		month = String.valueOf(mon);
		System.out.println(month);
		int yr =  TestData_Sheet.getCellDataInt(1, 2);
		year = String.valueOf(yr);
		System.out.println(year);
		int cvc =  TestData_Sheet.getCellDataInt(1, 3);
		cvcNumber = String.valueOf(cvc);
		System.out.println(cvcNumber);

		sendKeys("id", pmCardNumber, creditCardNumber);
		Thread.sleep(2000);
		
		sendKeys("xpath", pmExpiryMonth, month);
		Thread.sleep(2000);
				
		sendKeys("xpath", pmExpiryYear, year);
		Thread.sleep(2000);
		
		sendKeys("xpath", pmCVVNumber, cvcNumber);
		Thread.sleep(5000);
		Message = "Paypal_Payment - creditCardNumber_Month_Year_CVVNumber :- All payment details are correct";
		Success_FailureMessageSheet.storeMessageinfile(Message);
		
	}
	
	/**public static void useReferralCode(String xlxspath) throws Exception
	{
		TestData_Sheet.setExcelFile(xlxspath, "PaymentPart");
		creditCardNumber =  TestData_Sheet.getCellData(1,4);
		sendKeys("id", "promocode", referralCode);
		Thread.sleep(5000);
		clickElement("xpath", ".//*[@id='applyPromoCodeBtnid']");
		Thread.sleep(2000);
		String errorMessage = "Promo code you have entered is invalid.";  
		Boolean message = driver.findElement(By.xpath(".//*[@id='divAjaxNotification']")).isDisplayed();
		if(message == true)
		{	
			String getMessage = getText("xpath", ".//*[@id='divAjaxNotification']");
			if(errorMessage == getMessage )
			{
				clearElement("xpath", ".//*[@id='applyPromoCodeBtnid']");
				Thread.sleep(2000);
			}
		}
	}*/

	public static void finalPayment() throws Exception
	{
		clickElement("xpath", pmPaymentbutton);
		Thread.sleep(10000);
		
		String MessageText = "Paypal_Payment" + "final Payment:- "+ getText("xpath", pmPopupText);
		Success_FailureMessageSheet.storeMessageinfile(MessageText);
		Thread.sleep(2000);
		
		clickElement("xpath", pmPopupOKButton);
		Thread.sleep(10000);
		Reporter.log("User Booked Meeting- Successfully");
	}
		
	
}
