/**
 * 
 */
package Application_Module;

import org.openqa.selenium.By;
import Config.Actions_Class;
import Config.Success_FailureMessageSheet;

/**
 * @author Dell
 *
 */
public class MeetingBookingFullFlow extends Actions_Class
{
	public static String CancellationPolicy;
	public static String classname;
	public static void normalMeetingBookingFullFlow(String xlxspath, String date) throws Exception 
	{
		classname = "MeetingBookingFullFlow - normalMeetingBookingFullFlow";
		Search_Mentor.search_Mentor_Home(xlxspath);
		Thread.sleep(5000);
		Search_Mentor.open_Mentor_Detail();
		Thread.sleep(20000);
		Search_Mentor.open_Book_Meeting();
		Thread.sleep(5000);
		Book_Meeting.select_Meeting_Hours(xlxspath);
		Thread.sleep(2000);
		Book_Meeting.select_Slot01(date);
		Thread.sleep(5000);
		Book_Meeting.select_Slot_Time01(xlxspath);
		Thread.sleep(5000);
		Book_Meeting.meeting_Purpose(xlxspath);
		Thread.sleep(10000);
		
		// Store Cancellation policy type
		//CancellationPolicy =  getText("xpath", bmCancellationPolicy);
		
		Book_Meeting.proceedToPayment();
		Thread.sleep(8000);
		
		Paypal_Payment.checkMentorAmount(xlxspath);
		Paypal_Payment.creditCardNumber_Month_Year_CVVNumber(xlxspath);
		Thread.sleep(5000);
		Paypal_Payment.finalPayment();
		Thread.sleep(10000);
		
		boolean failMessage = driver.findElement(By.xpath(pmFailMessage)).isDisplayed();
		if(failMessage == true)
		{
			errorMessage("xpath", pmFailMessage, classname, failMessage);
		}
		
		else
		{
			System.out.println("Meeting Book Successfully");
			String Message = "Meeting Booking FullFlow - " + "Meeting book successfully";
			Success_FailureMessageSheet.storeMessageinfile(Message);
		}
	}
	
}
