/**
 * 
 */
package Application_Module;

import org.testng.Reporter;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.TestData_Sheet;

/**
 * @author welcome
 *
 */
public class Login_Module extends Actions_Class
{
	

	public static void WebAuthentication(String xlsxPath) throws Exception
	{
		TestData_Sheet.setExcelFile(xlsxPath,"Login");
		username =  TestData_Sheet.getCellData(1,0);
		password =  TestData_Sheet.getCellData(1,1);
	
		clearElement("id", webAuthUsername);
		sendKeys("id", webAuthUsername, username);
		clearElement("id", webAuthPassword);
		sendKeys("id", webAuthPassword, password);
		Thread.sleep(2000);
		clickElement("id", webAuthOkbutton);
		
		String FailMessage = getText("xpath", webAuthMessage);
		if(FailMessage != null)
		{
			String Message = "Web Authentication - Web Authentication done Successfully" + FailMessage;
			Success_FailureMessageSheet.storeMessageinfile(Message);
			Reporter.log(Message);
		}
		else
		{
			String Message = "Web Authentication - Web Authentication done Successfully";
			Success_FailureMessageSheet.storeMessageinfile(Message);
			Reporter.log("Web Authentication done successfully....");
		}
	}
					
	public static void userLogin(String xlsxPath) throws Exception
	{
		TestData_Sheet.setExcelFile(xlsxPath, "Login");
		emailaddress =  TestData_Sheet.getCellData(2,0);
		password =  TestData_Sheet.getCellData(2,1);
		
		clearElement("id", loginUsername);
		sendKeys("id", loginUsername, emailaddress);
		clearElement("id", loginPassword);
		sendKeys("id", loginPassword, password);
		Thread.sleep(5000);
		submitForm("xpath", loginOKbutton);
		Thread.sleep(5000);
		
		String FailMessage = getText("xpath", loginMessage);	
		if(FailMessage != null)
		{
			Message = "User Login - " + FailMessage;
			Success_FailureMessageSheet.storeMessageinfile(Message);
			Reporter.log(Message);
		}
		
		else
		{
			Message = "User Login - " + "Login Successfully";
			Success_FailureMessageSheet.storeMessageinfile(Message);
			System.out.println("Login Successfully...");
			Reporter.log("User login to website...");
		}
	}
	
	public static void mentorLogin(String xlxspath) throws Exception
	{
		TestData_Sheet.setExcelFile(xlxspath, "Login");
		emailaddress =  TestData_Sheet.getCellData(3,0);
		password =  TestData_Sheet.getCellData(3,1);
		clearElement("id", loginUsername);
		sendKeys("id", loginUsername, emailaddress);
		clearElement("id", loginPassword);
		sendKeys("id", loginPassword, password);
		Thread.sleep(5000);
		submitForm("xpath", loginOKbutton);
		//clickElement("xpath", ".//*[@id='frmLogin']/button");
		Thread.sleep(5000);
		String FailMessage = getText("xpath", loginMessage);
		if(FailMessage != null)
		{
			Message = "User Login - " + FailMessage;
			Success_FailureMessageSheet.storeMessageinfile(Message);
			Reporter.log(Message);
		}
		
		else
		{
			Message = "User Login - " + "Login Successfully";
			Success_FailureMessageSheet.storeMessageinfile(Message);
			System.out.println("Login Successfully...");
			Reporter.log("Mentor login to website...");
		}
		
	}
	
}
