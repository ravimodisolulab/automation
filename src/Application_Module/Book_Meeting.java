/**
 * 
 */
package Application_Module;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.TestData_Sheet;

/** 
 * This class for - Meeting Booking module....
 * it Include Below Modules in this file.
 * - Select Hours - Meeting Duration
 * - Select Slot - Meeting Date
 * - Select Time - Meeting Time
 * - Message - Meeting Purpose.
 * - Message - Meeting Details - Currently does not added in any cases.
 * - Click Proceed button.
 */
public class Book_Meeting extends Actions_Class
{
		public static void select_Meeting_Hours(String xlxspath) throws Exception
		{
			DataSheetFile(xlxspath, "BookMeeting");
			int s =  TestData_Sheet.getCellDataInt(1, 0);
			clickElement("xpath", bmSelectHours);
			dropdownByIndex("xpath",bmSelectHours, s);
			Thread.sleep(5000);
			System.out.println("Meeting hours selected.");
			
			Message = "Book Meeting - Meeting Hours is:- " + s;			
			Success_FailureMessageSheet.storeMessageinfile(Message);
		}
		
		public static void select_Slot01(String date) throws Exception
		{
		
			clickElement("xpath", bmSelectSlotDate);
			clearElement("xpath", bmSelectSlotDate);
			sendKeys("xpath", bmSelectSlotDate, date);
			System.out.println("Press / TAB / key");
			driver.findElement( By.xpath(bmSelectSlotDate)).sendKeys(Keys.TAB);
			Thread.sleep(5000);
			System.out.println("Press / TAB / key");
			Message = "Book Meeting - Meeting Date is:- " + date;			
			Success_FailureMessageSheet.storeMessageinfile(Message);
		}
		
		public static void select_Slot_Time01(String xlxspath) throws Exception
		{
			DataSheetFile(xlxspath, "BookMeeting");
			int slotIndex =  TestData_Sheet.getCellDataInt(1, 2);
			System.out.println(slotIndex);
			clickElement("xpath", bmSelectSlotTime);
			dropdownByIndex("xpath", bmSelectSlotTime, slotIndex);
			Thread.sleep(5000);
			
			Message = "Book Meeting - Slot index is:- " + slotIndex;			
			Success_FailureMessageSheet.storeMessageinfile(Message);
		}
		
		public static void meeting_Purpose(String xlxspath) throws Exception
		{
			DataSheetFile(xlxspath, "BookMeeting");
			MeetingPurpose =  TestData_Sheet.getCellData(1,3);
			sendKeys("xpath", bmEnterMeeting, MeetingPurpose);
			
			Message = "Book Meeting - Meeting Message is:- " + MeetingPurpose;			
			Success_FailureMessageSheet.storeMessageinfile(Message);
		}
		
		public static void meeting_Details(String xlxspath) throws Exception
		{
			TestData_Sheet.setExcelFile(xlxspath, "BookMeeting");
			MeetingDetails =  TestData_Sheet.getCellData(1,1);
			
			WebElement editorFrame = driver.findElement(By.id("ck"));
			driver.switchTo().frame(editorFrame);
			WebElement body = driver.findElement(By.tagName("body"));
			body.clear(); 
			body.sendKeys(MeetingDetails);
			//sendKeys("id", "ck", MeetingDetails);
		}
		
		public static void proceedToPayment() throws Exception
		{
			clickElement("xpath", bmProceedButton);
			Thread.sleep(10000);
			Reporter.log("Meeting Booking Detail entered and application redirect on payment page...");
			Message = "Book Meeting - Click on Proceed button :- Redirect on Payment Page....";			
			Success_FailureMessageSheet.storeMessageinfile(Message);
						
		}
}
