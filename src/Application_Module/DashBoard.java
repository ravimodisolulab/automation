package Application_Module;

import org.testng.Reporter;

import Config.Actions_Class;

public class DashBoard extends Actions_Class
{   
	public static String Amount = null;
	public static String Detail = null;
	public static String Type = null;
	public static String DateAndTime = null;
	
	public static String userDashboard(String xlxspath) throws Exception
	{
		Amount = getText("xpath", dboardUserAmount);
		Detail = getText("xpath", dboardUserDetail);
		Type = getText("xpath", dboardUserType);
		DateAndTime = getText("xpath", dboardUserDateAndTime);
		
		Reporter.log("User dashboard entry is like:-- " + Amount + " , " + Detail + " , " + Type + " , " + DateAndTime);
		return Amount;
		
	}
	
	public static String mentorDashboard() throws Exception
	{
		Amount = getText("xpath", dboardMentorAmount);
		Detail = getText("xpath", dboardMentorDetail);
		Type = getText("xpath", dboardMentorType);
		DateAndTime = getText("xpath", dboardMentorDateAndTime);
		
		Reporter.log("Mentor dashboard entry is like:-- " + Amount + " , " + Detail + " , " + Type + " , " + DateAndTime);
		return Amount;
	}
}

