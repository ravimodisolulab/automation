package Application_Module;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;

public class launchBrowser extends Actions_Class
{
	// Open chrome browser
		public static void chromeBrowser() throws Exception
		{
			System.setProperty("webdriver.chrome.driver", exeChromePath);
			driver = new ChromeDriver();
			
			Message = "Launch Browser - Chrome Browser - Chrome Browser Opened.... ";			
			Success_FailureMessageSheet.storeMessageinfile(Message);
			/**ChromeOptions chromeOptions = new ChromeOptions();
		    chromeOptions.addArguments("--start-maximized");
		    driver = new ChromeDriver(chromeOptions);*/
		}
		
		public static void fireFoxBrowser() throws Exception
		{
			
			System.setProperty("webdriver.gecko.driver", exeFireFoxPath);
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			Message = "Launch Browser - Firefox Browser - Mozila FireFox Browser Opened.... ";			
			Success_FailureMessageSheet.storeMessageinfile(Message);		
		}
		
		public static void maximizeBrowser() throws Exception
		{
			 
		}
}
