/**
 * 
 */
package Application_Module;

import org.testng.Reporter;

import Config.Actions_Class;
import Config.Objects_Repository;
import Config.TestData_Sheet;

/**
 * @author welcome
 *
 */
public class Search_Mentor extends Actions_Class
{
		public static void search_Mentor_Home(String xlxspath) throws Exception
		{
			Objects_Repository.DataSheetFile(xlxspath, "Login");
			mentor =  TestData_Sheet.getCellData(3,3);
			System.out.println(mentor);
			sendKeys("xpath", enterMentor, mentor);
			Thread.sleep(10000);
			clickElement("xpath", selectMentorFromlist);
			Thread.sleep(10000);
			System.out.println("Mentor profile open...");
			Reporter.log(mentor + "Profile Open...");
		}
		
		public static void open_Mentor_Detail() throws Exception
		{
			clickElement("xpath", clickMentorName);
			Thread.sleep(2000);
			System.out.println("Click on mentor name");
		}
		
		public static void open_Book_Meeting() throws Exception
		{
			clickElement("xpath", clickBookMeeting);
			Thread.sleep(2000);
			System.out.println("Open mentor book meeting page");
		}
}
