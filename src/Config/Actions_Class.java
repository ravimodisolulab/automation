/**
 * Action Class/Keyword Driven Class
 */
package Config;

import java.util.Set;
import java.util.logging.Logger;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author welcome
 *
 */
public class Actions_Class extends Objects_Repository
{
	
		  @SuppressWarnings("unused")
		  private static Logger Log = Logger.getLogger(Logger.class.getName());
		  public static WebDriver driver;
		  public String baseUrl;
		  public boolean acceptNextAlert = true;
		  public StringBuffer verificationErrors = new StringBuffer();
		  	
	  	public static By Byelement (String Type, String Path)
	  	{
	  		if (Type.equals("id"))
	  		{
	  			return By.id(Path);
	  		}
	  		else if (Type.equals("className"))
	  		{
	  			return By.className(Path);
	  		}
	  		else if (Type.equals("name"))
	  		{
	  			return By.name(Path);
	  		}
	  		
	  		else if (Type.equals("xpath"))
	  		{
	  			return By.xpath(Path);
	  		}
	  		else if (Type.equals("tagName"))
	  		{
	  			return By.tagName(Path);
	  		}
	  		else if (Type.equals("cssSelector"))
	  		{
	  			return By.cssSelector(Path);
	  		}
	  		
	  		else if (Type.equals("linkText"))
	  		{
	  			return By.linkText(Path);
	  		}
	  		else if (Type.equals("link"))
	  		{
	  			return By.linkText(Path);
	  		}
	  		
	  		else
	  		{
	  			return By.id(Path);
	  		}
	  	}
	  	
	  	public static void closeWindow()
	  	{
	  		driver.quit();
	  	}
	  	public static void clickElement(String Type,String Path)
	  	{
	  		driver.findElement(Byelement(Type, Path)).click(); 	
	  	}
	  	
	  	public static WebElement calldriver(String Type,String Path)
	  	{
	  		return driver.findElement(Byelement(Type, Path)); 	
	  	}
	  	
	  	public static java.lang.String getmessage(String Type,String Path)
	  	{
	  		return driver.findElement(Byelement(Type, Path)).getText();
	  		
	  	}
	  	
	  	public static void clearElement(String Type,String Path)
	  	{
	  		driver.findElement(Byelement(Type, Path)).clear(); 	
	  	}
	  	
	  	public static void sendKeys(String Type,String Path,String key)
	  	{
	  		driver.findElement(Byelement(Type, Path)).sendKeys(key);	
	  	}
	  	
	  	public static  void sendKeysIntValue(String Type,String Path,CharSequence[] key)
	  	{
	  		driver.findElement(Byelement(Type, Path)).sendKeys(key);
	  	}
	  	
	  	public static WebElement hover(String Type,String String)
	  	{
	  		return driver.findElement(Byelement(Type, String));
	  	}
	  	
	  	public static void hoverclick(String Type,String Path)
	  	{
	  		driver.findElement(Byelement(Type, Path)).click(); 
	  	}
	  	
	  	public static void refresh()
	  	{
	  	  driver.navigate().refresh();
	  	}
	  	
	  	public static void back()
	  	{
	  	  driver.navigate().back();
	  	}
	  	
	  	public static void forward()
	  	{
	  	  driver.navigate().forward();
	  	}
	  	
	  	public static Alert alertSwitch(String id, String Path) throws Exception
	  	{
	  	  clickElement("xpath", ".//*[@id='makePayment']");
	  	  Alert simpleAlert = driver.switchTo().alert();
	  	  String alertText = simpleAlert.getText();
	  	  System.out.println("Alert text is " + alertText);
	  	  simpleAlert.accept();
	  	  Thread.sleep(5000);
	  	  return simpleAlert;
	  	}
	  	
	  	public static String getAtribute(String Type, String Path, String Value)
	  	{
	  	  return driver.findElement(Byelement(Type, Path)).getAttribute(Value);
	  	}
	  	
	  	public static void submitForm(String Type,String Path)
	  	{
	  	  driver.findElement(Byelement(Type, Path)).submit();
	  	}
	  	
	  	public static void openURL(String url)
	  	{
	  	  driver.get(url);
	  	}
	  	
	  	public static String currentURL()
	  	{
	  		return driver.getCurrentUrl();
	  	  
	  	}
	  	
	  	public static String getText(String Type,String Path)
	  	{
	  	  return driver.findElement(Byelement(Type, Path)).getText();
	  	}
	  	
	  	public static WebElement webElementClass(String Type, String Path)
	  	{
	  	  return driver.findElement(Byelement(Type, Path));
	  	}
	  	
	  	public static void actionClass(WebElement element)
	  	{
	  	  
	  		Actions action = new Actions(driver);
	  	    action.moveToElement(element).build().perform();
			clickElement("xpath",".//*[@id='flUserProfileImage']");

	  	}
	  	
	  	public static  void WebDriverExplicitWait(String Type, String Path)
	  	{
	  		
	  		WebDriverWait wait = new WebDriverWait(driver, 10);
	  		@SuppressWarnings("unused")
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(Byelement(Type, Path)));	  
	  	}
		public static  void WebDriverFluentWait(String Type, String Path)
	  	{
	  		
	  		 
	  	}
	  	
	  	public static void switchToWindow(String value) throws Exception
	  	{
	  		driver.switchTo().window(value);
	  	}
	  	
	  	public static  String windowHandle() throws Exception
	  	{
	  		return driver.getWindowHandle();
	  	}
	  	
	  	public static  Set<String> windowHandles() throws Exception
	  	{
	  		return driver.getWindowHandles();
	  	}
	  	
	  	public static void dropdownByIndex(String Type, String path, int Index) 
	  	{  
	  	 Select select = new Select (driver.findElement(Byelement(Type, path)));  
	  	 select.selectByIndex(Index);  
	  	 select.getOptions();  
	  	} 
	  	
	  	public static void dropdownByText(String Type, String path, String Text) 
	  	{  
	  	 Select select = new Select (driver.findElement(Byelement(Type, path)));  
	  	 select.getOptions();  
	  	 select.selectByVisibleText(Text); 
	  	}  
	  	
	  	public static void dropdownByValue(String Type, String path, String Value) 
	  	{  
	  	 Select select = new Select (driver.findElement(Byelement(Type, path)));  
	  	 select.getOptions();  
	  	 select.selectByValue(Value);  
	  	}
	  
	  	public static boolean errorMessage(String path, String value , String classname, boolean message) throws Exception 
	    {
	  		
	  		if(message == true)
			{
				System.out.println(message);
				String invalidMessage = getText(path, value);
				String Message =  classname + invalidMessage;
				Success_FailureMessageSheet.storeMessageinfile(Message);
				return message;
			}
	  		else
	  		{
	  			return message;
	  		}
	    }
}
