package Config;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class generalFunctions 
{
	public static String className = "Payment Varification with data base.";
	public static void sendEmailReport(String testcasename) throws Exception 
	{
			// TODO Auto-generated method stubs
			final String username = "hitesh.solulab@gmail.com";
			final String password = "hiteshk26";
			
			
			 String emailable_report =  Objects_Repository.reportpath+ "emailable-report.html";
			 String index_report =  Objects_Repository.reportpath + "index.html";
	
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
	
			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() 
			  {
				protected PasswordAuthentication getPasswordAuthentication() 
				{
					return new PasswordAuthentication(username, password);
				}
			  });
	
			try {
	
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress("hitesh.solulab@gmail.com"));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse
				(
						"utpal@solulab.com, milind@solulab.com, hitesh@solulab.com, harsh@solulab.com, chintan@solulab.com, prasaman@gmail.com" 
				));
				
				message.setSubject("Log File:- " + testcasename);
				
				
				// Create the message part
		         BodyPart messageBodyPart = new MimeBodyPart();
	
		         // Now set the actual message
		         messageBodyPart.setText("Hello, This is Wissenx Automation Testing....");
	
		         // Create a multipar message
		         Multipart multipart = new MimeMultipart();
	
		         // Set text message part
		         multipart.addBodyPart(messageBodyPart);
	
		         // Part two is attachment
		         messageBodyPart = new MimeBodyPart();   
		         
		         
				addAttachment(multipart, emailable_report);
				Thread.sleep(2000);
				addAttachment(multipart, index_report);
		         
		         // Send the complete message parts
		         message.setContent(multipart);
				
	
				Transport.send(message);
	
				System.out.println("Done");
	
			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}


	}
	private static void addAttachment(Multipart multipart, String filename) throws Exception
	{
	    DataSource source = new FileDataSource(filename);
	    BodyPart messageBodyPart = new MimeBodyPart();        
	    messageBodyPart.setDataHandler(new DataHandler(source));
	    messageBodyPart.setFileName(filename);
	    multipart.addBodyPart(messageBodyPart);
	}
	
		static Date dt = new Date();
	public static String dateclasses(int addDay)
	{
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
			
			Calendar c = Calendar.getInstance(); 
			//c.setTime(dt); 
			c.add(Calendar.DATE, addDay);
			dt = c.getTime();
			String date = sdfDate.format(dt);
			return date;
				
		
	}
	
	public static String timeClasses(int addTime)
	{
			SimpleDateFormat df = new SimpleDateFormat("HH:MM");
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MINUTE, addTime);
			// 24 hours format
			
			String time = df.format(c.getTime());
			
			return time;
			
	}

}

