/**
 * 
 */
package Config;

import java.sql.ResultSet;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Wait;

/**
 * @author welcome
 *
 */
public class Objects_Repository 
{
	// Selenium commands
	public static WebDriver driver;
	public static WebElement element;
	@SuppressWarnings("rawtypes")
	public static Wait wait;
	
	
	//General
	public static String url = "https://dev.wissenx.com";
	public static String exeChromePath = "D:\\Automation Requirements\\chromedriver\\chromedriver.exe";
	public static String exeFireFoxPath = "D:\\Automation Requirements\\geckodriver.exe";
	public static String reportpath = "D:\\Projects\\wissenxDevServer\\test-output\\";
	
	
	// API List
	
	public static String meetingcompleted = "https://dev.wissenx.com/hitesh@solulab.com";
	public static String noShowByUser = "https://dev.wissenx.com/noshowbyuserapi/hitesh@solulab.com";
	public static String noShowByMentor = "https://dev.wissenx.com/noshowbymentorapi/hitesh@solulab.com";
	public static String noShowByBoth = "https://dev.wissenx.com/noshowbybothapi/hitesh@solulab.com";
	public static String meetingtimeOut = "https://dev.wissenx.com/timoutapi/hitesh@solulab.com";
	public static String fullRefund = "https://dev.wissenx.com/fullrefund/hitesh@solulab.com";
	public static String partialRefund = "https://dev.wissenx.com/partialrefund/hitesh@solulab.com/200";
	
	// Variable List which is used in function
	public static String emailaddress;
	public static String password;
	public static String username;
	public static String mentor;
	public static String MeetingPurpose;
	public static String MeetingDetails;

	public static String creditCardNumber;
	public static String month;
	public static String year;
	public static String cvcNumber;
	public static String startDate;
	public static String endDate;
	public static String startTime;
	public static String endTime;
	public static String Message;
	public static String MentorRate;
	public static String WissensCharge;
	public static String Taxes;
	public static String TotalCharge;
	public static ResultSet rs;
	
	//xpath varialbe for navigation flow
	public static String clickOnLogin = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[4]/a";
	public static String clickOnMenu = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[3]/a";
	public static String mentorLogout = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[3]/ul/li[5]/a";
	public static String userLogout = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[3]/ul/li[6]/a";
	
	//xpath variable - For web authentication & Login Form
	public static String webAuthUsername = "txtWissenXUserName";
	public static String webAuthPassword = "txtWissenXPassword";
	public static String webAuthOkbutton = "btnCheckWissenXUserCredentials";
	public static String webAuthMessage = ".//*[@id='divAjaxNotification']";
	public static String loginUsername = "email";
	public static String loginPassword = "password";
	public static String loginOKbutton = ".//*[@id='frmLogin']/button";
	public static String loginMessage = "html/body/div[2]/div[2]/div/div/div[1]";
	
	// xpath variable - For Search mentor
	public static String enterMentor = ".//*[@id='txtQueryString']";
	public static String selectMentorFromlist = ".//*[@id='eac-container-txtQueryString']/ul/li[1]/div";
	public static String clickMentorName = ".//*[@class='blog-title']";
	public static String clickBookMeeting = ".//*[@class='btn-2 js-BookSlotForThisMentor']";
	
	// xpath variable for Book meeting page
	public static String bmSelectHours = ".//*[@id='call_duration']";
	public static String bmSelectSlotDate = ".//*[@id='slot1_date']";
	public static String bmSelectSlotTime = ".//*[@id='slot1_time']";
	public static String bmEnterMeeting = ".//*[@id='bookSlotForm']/div[2]/div/div[1]/input";
	public static String bmProceedButton = ".//*[@id='makePayment']";
	public static String bmCancellationPolicy = ".//*[@id='bookSlotForm']/div[2]/div/div[3]/div[2]/div/label";
	
	// xpath variable for payment screen
	public static String pmMentorRate = ".//*[@id='mentor_rate']";
	public static String pmWissenxCharge = ".//*[@id='wissenx_charge']";
	public static String pmWissenxTaxes = "";
	public static String pmTotalPayment = ".//*[@id='totalpayable']";
	public static String pmCardNumber = "cardNumber";
	public static String pmExpiryMonth = ".//*[@id='cardExpiryMM']";
	public static String pmExpiryYear = ".//*[@id='cardExpiryYY']";
	public static String pmCVVNumber = ".//*[@id='cardCVC']";
	public static String pmFailMessage = ".//*[@id='divAjaxNotification']";
	public static String pmPaymentbutton = ".//*[@id='successfulMeetingModalId']";
	public static String pmPopupText = ".//*[@id='successfulMeetingModal']/div/div/div[2]";
	public static String pmPopupOKButton = ".//*[@id='makePayment']";
	
	
	// xpath variable for MyAccount Menu	
	public static String maMenu = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[1]/a";
	public static String maMenuMentorDashboard = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[1]/ul/li[1]/a";
	public static String maMenuMentorCalendar = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[1]/ul/li[2]/a";
	public static String maMenuMentorMyInvite = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[1]/ul/li[4]/a";
	public static String maMenuUserMyInvite = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[1]/ul/li[2]/a";
	public static String maMenuMentorMyMeeting = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[1]/ul/li[5]/a";
	public static String maMenuUserMyMeeting = ".//*[@id='bs-example-navbar-collapse-1']/ul/li[1]/ul/li[3]/a";
	
	// xpath variable fro Dashboard
	public static String dboardUserAmount = ".//*[@id='datatable']/tbody/tr[1]/td[1]/span";
	public static String dboardUserDetail = ".//*[@id='datatable']/tbody/tr[1]/td[2]";
	public static String dboardUserType = ".//*[@id='datatable']/tbody/tr[1]/td[3]";
	public static String dboardUserDateAndTime = ".//*[@id='datatable']/tbody/tr[1]/td[4]";
	public static String dboardMentorAmount = ".//*[@id='datatable']/tbody/tr[1]/td[1]/span";
	public static String dboardMentorDetail = ".//*[@id='datatable']/tbody/tr[1]/td[2]";
	public static String dboardMentorType = ".//*[@id='datatable']/tbody/tr[1]/td[3]";
	public static String dboardMentorDateAndTime = ".//*[@id='datatable']/tbody/tr[1]/td[4]";
	
	// xpath variable for My Invites
	public static String miReceivedTab = "html/body/div[3]/div[4]/div/ul/li[1]/a";
	public static String miSentTab = "html/body/div[3]/div[4]/div/ul/li[2]/a";
	public static String miDeclineNormalFromReceive = ".//*[@class='btn btn-darkgray btn-social mr10 mb10 declineInvite']";
	public static String miDeclinePrivateFromReceive = ".//*[@class='btn btn-darkgray btn-social mr10 mb10 declineInvitePrivate']";
	public static String miReceiveSelectSlot = ".//*[@class='mt2 mr5 invitationSlotSelected']";
	public static String miNormalAcceptFromReceive = ".//*[@class='btn red-btn btn-social mr10 mb10 acceptInvite']";
	public static String miDeclineNormalFromReceivePopupOk = ".//*[@id='mentor_reasonbtn']";
	public static String miDeclinePrivateFromReceivePopupOk = ".//*[@id='user_reasonbtn']";
	public static String miPrivateAcceptFromReceive = ".//*[@class='btn red-btn btn-social mr10 mb10 acceptInvitePrivate']";
	public static String miCancellNoramlFromSent = ".//*[@class='cancel cancelhide btn red-btn btn-social mr10 mb10']";
	public static String miCancellPrivateFromSent = ".//*[@class='cancel cancelhide btn red-btn btn-social mr10 mb10']";
	public static String miReceiveTabMeetingTime = ".//*[@class='mt2 mr5 invitationSlotSelected']";
	public static String miSentTabMeetingTime = ".//*[@class='slot']/li/b";
	
	//xpath variable for My Meetings
	public static String mmFutureTab = "html/body/div[3]/div/div[2]/div[3]/div/ul/li[1]/a";
	public static String mmCancellMeetingByUSer = ".//*[@class='buttons btn-black cancelMeetingUser']";
	public static String mmCancellMeetingByMentor = ".//*[@class='buttons btn-black cancelMeetingMentor']";
	public static String mmMeetingID = ".//*[@id='tab_future']/div/div/div[2]/div/div[2]/div/div[2]/a";
	public static String mmMeetingStatusAtFuture = ".//*[@id='tab_future']/div/div/div[2]/div/div[1]/div/div[2]/label";
	
	public static String mmCancelMeetingPopupMentorText = ".//*[@id='cancelMeetingMentor']/div/div/div[2]/div[1]/label";
	public static String mmCancelMeetingPopupMentorOK = ".//*[@id='cancelMeetingMentorOkBtn']";
	public static String mmCancelMeetingPopupUserText = ".//*[@id='cancelMeetingUser']/div/div/div[2]/div[1]/label";
	public static String mmCancelMeetingPopupUserOK = ".//*[@id='cancelMeetingUserOkBtn']";
	
	public static String mmCancelMeetingResonPopupMentorOK = ".//*[@id='mentor_reasonbtn']";
	public static String mmCancelMeetingResonPopupMentorText = ".//*[@id='mentorradiofirst']";
	public static String mmCancelMeetingResonPopupUserOk = ".//*[@id='user_reasonbtn']";
	public static String mmCancelMeetingResonPopupUserText = ".//*[@id='userradiofirst']";
	
	public static String mmCompletedTab = "html/body/div[3]/div/div[2]/div[3]/div/ul/li[2]/a";
	public static String mmRefundRequest = "";
	public static String mmMeetingTimeOutAt = "";
	public static String mmMeetingCancellAt = "";
	public static String mmMeetingStatusAtCompleted = "";
	
	
	// xpath variable for Mentor Calendar
	public static String mcaddAvailability = ".//*[@id='addAvailButton']";
	public static String mcsendPrivateInvite = ".//*[@id='sendPrivateButton']";
	
	// xpath variable for Mentor Calendar Add Availability
	public static String mcastartDateSlot = ".//*[@id='startDate']";
	public static String mcaendDateSlot = ".//*[@id='endDate']";
	public static String mcastartTimeSlot = ".//*[@id='timepicker1']";
	public static String mcaendTimeSlot = ".//*[@id='timepicker2']";
	public static String mcaaddAvailabilityPopupOK = ".//*[@id='addbtn']/button";
	
	public static String mcpiClickSelectUser = ".//*[@id='select2-lstUserList-container']";
	public static String mcpiSearchUser = "/html/body/span/span/span[1]/input";
	public static String mcpiSelectUser = ".//*[@class='select2-results__option select2-results__option--highlighted']";
	public static String mcpiSelectMeetingDuration = ".//*[@id='call_duration']";
	public static String mcpiSelectStartDate = ".//*[@id='callDate1']";
	public static String mcpiSelectStartTime = ".//*[@id='privateInviteTimePicker1']";
	public static String mcpiMessage = ".//*[@id='sendInvite']/div[1]/div[10]/textarea";
	public static String mcpiSendInvite = ".//*[@id='sendInvite']/div[2]/button";
	
	
	
	
	/**
	 * DataSheetFile is used in all method which include excel operation.
	 * @param xlsxPath
	 * @param SheetName
	 * @throws Exception
	 */
	
	public static void DataSheetFile(String xlsxPath, String SheetName) throws Exception
	{
		TestData_Sheet.setExcelFile(xlsxPath, SheetName);
	}
	
}
