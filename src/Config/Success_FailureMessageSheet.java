package Config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Success_FailureMessageSheet 
{
	 private static final String newLine = System.getProperty("line.separator");
	 static SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
	 static Date now = new Date();
	 static String date = sdfDate.format(now);
	 static String fileName = Objects_Repository.reportpath + date + ".txt";
	 
	public static void storeMessageinfile(String Message) throws Exception
	{
	
		 
		
		PrintWriter printWriter = null;
	    File file = new File(fileName);
	    try 
	    {
	        if (!file.exists()) file.createNewFile();
	        printWriter = new PrintWriter(new FileOutputStream(fileName, true));
	        printWriter.write(newLine + Message);
	    } 
	    catch (IOException ioex) 
	    {
	        ioex.printStackTrace();
	    } finally 
	    {
	        if (printWriter != null) 
	        {
	            printWriter.flush();
	            printWriter.close();
	        }
	    }
	}
	
	public static void deleteFile() throws Exception
	{
		try 
		{ 
	         File file = new File(fileName);
	         if(file.delete()) 
	         { 
	            System.out.println(file.getName() + " is deleted!");
	         } 
	         else 
	         {
	            System.out.println("Delete operation is failed.");
	    	 }
	      } 
		catch(Exception e) 
		{
	         e.printStackTrace();
	    }
	   
	}
}
