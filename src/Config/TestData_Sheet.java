package Config;

import java.io.FileInputStream;
import java.util.Date;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TestData_Sheet 
{
		private static XSSFSheet ExcelWSheet;
		private static XSSFWorkbook ExcelWBook;
		private static XSSFCell Cell;
		@SuppressWarnings("unused")
		private static XSSFRow Row;

		//This method is to set the File path and to open the Excel file, Pass Excel Path and Sheetname as Arguments to this method
		public static void setExcelFile(String Path,String SheetName) throws Exception 
		{

			try 
			{	// Open the Excel file
				FileInputStream ExcelFile = new FileInputStream(Path);
				// Access the required test data sheet
				ExcelWBook = new XSSFWorkbook(ExcelFile);
				ExcelWSheet = ExcelWBook.getSheet(SheetName);
			} 
			catch (Exception e)
			{
				throw (e);
			}

		}

		//This method is to read the test data from the Excel cell, in this we are passing parameters as Row num and Col num
		public static String getCellData(int RowNum, int ColNum) throws Exception
		{
			try
			{
				Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
				String CellData = Cell.getStringCellValue();
				return CellData;
			}
			catch (Exception e)
			{
				return"";
			}

		}
		public static int getCellDataInt(int RowNum, int ColNum) throws Exception
		{
			try
			{
				
				Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);					
				int CellData = (int) Cell.getNumericCellValue();
				
				return CellData;
			}
			catch (Exception e)
			{
				return 0;
			}

		}
		
		public static Boolean getBooleanCellData(int RowNum, int ColNum) throws Exception
		{
			try
			{
				
				Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);					
				Boolean CellData = Cell.getBooleanCellValue();
				
				return CellData;
			}
			catch (Exception e)
			{
				return false;
			}

		}
		
		public static Date geDateCellData(int RowNum, int ColNum) throws Exception
		{
		
			try
			{
				
				Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);					
				Date date = Cell.getDateCellValue();
				
				return date;
			}
			catch (Exception e)
			{
				return null;
			}
			

		}
		

	/**	//This method is to write in the Excel cell, Row num and Col num are the parameters
		
	public static void setCellData(String Result, String Path_TestData) throws Exception	{
		
		try{
		
			int RowNum;
			int ColNum;
			
			
			Row  = ExcelWSheet.getRow(RowNum);
		
			Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
		
			if (Cell == null) {
		
				Cell = Row.createCell(ColNum);
		
				Cell.setCellValue(Result);
		
			} 
			else 
			{
		
				Cell.setCellValue(Result);
		
			}
		
		// Constant variables Test Data path and Test Data file name
			 SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
			 Date now = new Date();
			FileOutputStream fileOut = new FileOutputStream(Path_TestData + File_TestData);
		
				ExcelWBook.write(fileOut);
		
				fileOut.flush();
		
				fileOut.close();
		
			}catch(Exception e){
		
					throw (e);
		
			}
		
			}*/
}
