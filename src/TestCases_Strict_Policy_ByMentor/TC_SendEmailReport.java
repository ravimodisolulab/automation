package TestCases_Strict_Policy_ByMentor;

import org.testng.annotations.Test;

import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.generalFunctions;

public class TC_SendEmailReport extends Actions_Class
{

		
		@Test(priority = 0)
		public static void EmailReportExecute() throws Exception 
		{
			String testcasename = "TestCases_Strict_Policy_ByMentor";
			generalFunctions.sendEmailReport(testcasename);
		}
		
		public static void deleteLogFile() throws Exception 
		{
			Success_FailureMessageSheet.deleteFile();
		}
		
}
