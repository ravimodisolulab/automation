package TestCases_Strict_Policy_ByMentor;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import org.testng.Reporter;
import Application_Module.Login_Module;
import Application_Module.MentorCalendarPage;
import Application_Module.MyAccountMenu;
import Application_Module.MyInvites;
import Application_Module.Navigates_Modules;
import Application_Module.launchBrowser;
import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.TestData_Sheet;
import Config.generalFunctions;

import java.util.concurrent.TimeUnit;

public class TC_PM005_Cancell_ByMentor_Before_Accept_ByUser extends Actions_Class
{

	  public static String xlsxpath = "D:\\Projects\\inputFileFolder\\TestData_Sheet.xlsx";
	  @BeforeClass
	  public void beforeClass() throws Exception 
	  {
			  Success_FailureMessageSheet.storeMessageinfile("**********************************************************");
			  String classname = "****===> Private Meeting - Set By Mentor - Before Accept By User - Cancell By Mentor <===***";
			  Success_FailureMessageSheet.storeMessageinfile(classname);
			  Success_FailureMessageSheet.storeMessageinfile("**********************************************************");
		      Thread.sleep(5000);
		    Thread.sleep(5000);
		  	launchBrowser.chromeBrowser();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		  	Navigates_Modules.openwebsite();
			Login_Module.WebAuthentication(xlsxpath);
			Thread.sleep(10000);
	  }
	  
	  @Test
	  public void MentorLogin_and_Send_PrivateInvite() throws Exception
	  {
		  // Mentor Login and send private invite to user and cancell by user		
		  
		  Reporter.log("Mentor login to system, book meeting and logout from system....");
		  Navigates_Modules.RedirectToLogin(); Thread.sleep(5000);
		  Login_Module.mentorLogin(xlsxpath); Thread.sleep(2000);
		  MyAccountMenu.openMentorCalendar(); Thread.sleep(2000);
		  
		  DataSheetFile(xlsxpath, "BookMeeting");
		  int i = TestData_Sheet.getCellDataInt(1, 1);
		  String startDate = generalFunctions.dateclasses(i);
		  String startTime = generalFunctions.timeClasses(240);
		  Reporter.log("Book Meeting Date:-- " + startDate + startTime);
		  MentorCalendarPage.sendPrivateInviteToUser(xlsxpath, startDate, startTime);
		  
		  System.out.println("Private Invite send successfully...."); 
		  Thread.sleep(2000);
		  
		  MyAccountMenu.openMentorMyInvite();
		  MyInvites.declineInvitationFromSentListByMentor(); 
		  Thread.sleep(5000);
		  System.out.println("Private Invite Cancell successfully...."); 
		  Thread.sleep(5000);
		  Success_FailureMessageSheet.storeMessageinfile("Private Invite_before Accept_ByUser_Cancell_ByMentor == Run Successfully...");
		  Reporter.log("Mentor Cancell private invite before accept it by user...");
	  }
	
	  @AfterClass
	  public void afterClass() throws Exception
	  {
		  driver.quit();
	  }

}
