package TestCases_Strict_Policy_ByMentor;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import Application_Module.Login_Module;
import Application_Module.MentorCalendarPage;
import Application_Module.MyAccountMenu;
import Application_Module.Navigates_Modules;
import Application_Module.launchBrowser;
import Config.Actions_Class;
import Config.TestData_Sheet;
import Config.generalFunctions;
import java.util.concurrent.TimeUnit;

public class TC_PM009_MentorCreateAddAvailability extends Actions_Class
{
	
		  public static String xlsxpath = "D:\\Projects\\inputFileFolder\\TestData_Sheet.xlsx";
		  				
		  @BeforeClass
		  public void launchBrowser() throws Exception
		  {
			    Thread.sleep(5000);
			  	launchBrowser.chromeBrowser();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			  	Navigates_Modules.openwebsite();
				Login_Module.WebAuthentication(xlsxpath);
				Thread.sleep(10000);
		  }
		  
		  @Test (priority = '0')
		  public void Menor_AddAvialability() throws Exception 
		  {
			  Navigates_Modules.RedirectToLogin();
			  Thread.sleep(5000);
			  Login_Module.mentorLogin(xlsxpath);
		 
			  String startDate = generalFunctions.dateclasses(0);
			  DataSheetFile(xlsxpath, "BookMeeting");
			  int i = TestData_Sheet.getCellDataInt(1, 1);
			  String endDate = generalFunctions.dateclasses(i);
			  String currentTime = generalFunctions.timeClasses(60);
			  String futureTime = "23:45";
			  
			  MyAccountMenu.openMentorCalendar();
			  Thread.sleep(5000);
			  MentorCalendarPage.addAvailability(startDate, endDate, currentTime, futureTime);
			
			  
		  }
		  
		  @AfterClass
		  public void closeDriver() 
		  {
			  driver.quit();
		  }

}
