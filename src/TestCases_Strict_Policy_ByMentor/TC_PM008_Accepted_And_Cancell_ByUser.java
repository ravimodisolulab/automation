package TestCases_Strict_Policy_ByMentor;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import org.testng.Reporter;
import Application_Module.Login_Module;
import Application_Module.MentorCalendarPage;
import Application_Module.MyAccountMenu;
import Application_Module.MyInvites;
import Application_Module.MyMeeting;
import Application_Module.Navigates_Modules;
import Application_Module.Paypal_Payment;
import Application_Module.launchBrowser;
import Application_Module.paymentVarificationWithDatabase;
import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.TestData_Sheet;
import Config.generalFunctions;

import java.util.concurrent.TimeUnit;

public class TC_PM008_Accepted_And_Cancell_ByUser extends Actions_Class
{
  public static String xlsxpath = "D:\\Projects\\inputFileFolder\\TestData_Sheet.xlsx";
	  
	  @BeforeClass
	  public void beforeClass() throws Exception 
	  {
			  Success_FailureMessageSheet.storeMessageinfile("**********************************************************");
			  String classname = "****===> Private Meeting - Set By Mentor - Accept By User - Cancell By User <===***";
			  Success_FailureMessageSheet.storeMessageinfile(classname);
			  Success_FailureMessageSheet.storeMessageinfile("**********************************************************");
		      Thread.sleep(5000);
		      
		  	launchBrowser.chromeBrowser();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		  	Navigates_Modules.openwebsite();
			Login_Module.WebAuthentication(xlsxpath);
			Thread.sleep(10000);
	  }
	  
	  @Test
	  public void Private_Invite_Accepted_And_Cancell_ByUser() throws Exception 
	  {
		  // Mentor Login and send private invite to user and logout from app.
		  Reporter.log("Mentor Login to system, Send private invite to user and logout from system");
		  Navigates_Modules.RedirectToLogin(); Thread.sleep(5000);
		  Login_Module.mentorLogin(xlsxpath); Thread.sleep(2000);
		  MyAccountMenu.openMentorCalendar(); Thread.sleep(2000);
		  
		  DataSheetFile(xlsxpath, "BookMeeting");
		  int i = TestData_Sheet.getCellDataInt(1, 1);
		  String startDate = generalFunctions.dateclasses(i);
		  String startTime = generalFunctions.timeClasses(240);
		  Reporter.log("Book Meeting Date:-- " + startDate + startTime);
		  MentorCalendarPage.sendPrivateInviteToUser(xlsxpath, startDate, startTime);
	  
		  Navigates_Modules.RedirectToMentorLogout();
		  Thread.sleep(5000);
		  
		  Reporter.log("User login to system and accept private invite...");
		  // User Login and Accept Private Invite From My Invite and cancell From My Meeting
		  Navigates_Modules.RedirectToLogin();
		  Thread.sleep(5000);
		  Login_Module.userLogin(xlsxpath);
		  MyAccountMenu.openUserMyInvite(); Thread.sleep(2000);
		  MyInvites.acceptInvitationFromReceivedByUser(); Thread.sleep(2000);
		  Paypal_Payment.checkMentorAmount(xlsxpath);Thread.sleep(2000);
		  Paypal_Payment.creditCardNumber_Month_Year_CVVNumber(xlsxpath); Thread.sleep(2000);
		  Paypal_Payment.finalPayment(); Thread.sleep(5000);
		  Reporter.log("After accept private invite check payment with database");
		  paymentVarificationWithDatabase.afterAcceptPrivateInvite_ByUser(xlsxpath);
		 	  
		  MyAccountMenu.openUserMyMeeting();
		  Thread.sleep(5000);
		  MyMeeting.cancell_Meeting_ByUser();
		  Thread.sleep(5000);
		  Reporter.log("AFter cancell accepted meeting - check payment with database");
		  paymentVarificationWithDatabase.afterAcceptPrivateInvite_CancellByUser_CheckPayment(xlsxpath);
		  Thread.sleep(5000);
		  Success_FailureMessageSheet.storeMessageinfile("Private Invite Accept_By User and Cancell By User -- Run successfully...");
	  }
	
	  @AfterClass
	  public void afterClass() 
	  {
		  driver.quit();
	  }


}
