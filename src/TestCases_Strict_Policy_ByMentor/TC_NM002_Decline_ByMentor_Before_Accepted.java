package TestCases_Strict_Policy_ByMentor;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import org.testng.Reporter;
import Application_Module.Login_Module;
import Application_Module.MeetingBookingFullFlow;
import Application_Module.MyAccountMenu;
import Application_Module.MyInvites;
import Application_Module.Navigates_Modules;
import Application_Module.launchBrowser;
import Application_Module.paymentVarificationWithDatabase;
import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.TestData_Sheet;
import Config.generalFunctions;

import java.util.concurrent.TimeUnit;

public class TC_NM002_Decline_ByMentor_Before_Accepted extends Actions_Class
{
 
	
  public static String xlxspath = "D:\\Projects\\inputFileFolder\\TestData_Sheet.xlsx";
  
  
  @BeforeClass
  public void beforeClass() throws Exception 
  {
	  Success_FailureMessageSheet.storeMessageinfile("**********************************************************");
	  String classname = "****===> Normal Meeting Set By User - Mentor Decline it from My Invite Receive Tab <===***";
	  Success_FailureMessageSheet.storeMessageinfile(classname);
	  Success_FailureMessageSheet.storeMessageinfile("**********************************************************");

	  launchBrowser.chromeBrowser();
	  Navigates_Modules.openwebsite();
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  Login_Module.WebAuthentication(xlxspath);
	  Thread.sleep(10000);
  }
  
  @Test (priority = 0)
  public static void NormalInvite_CancelByMentor_BeforeAcceptByMentor() throws Exception 
  {
	 	// User Login, Book Meeting and Logout from website
	     Reporter.log("User login to the system book meeting and logout from system...");
	  	 Navigates_Modules.RedirectToLogin();
		 Thread.sleep(2000);
		 Login_Module.userLogin(xlxspath);
		 Thread.sleep(5000);
		 DataSheetFile(xlxspath, "BookMeeting");
		 int i = TestData_Sheet.getCellDataInt(1, 1);
		 String startDate = generalFunctions.dateclasses(i);
		 Reporter.log("Meeting Booking Date:--" + startDate);
		 Thread.sleep(2000);
		 // Meeting Booking By User
		 
		 MeetingBookingFullFlow.normalMeetingBookingFullFlow(xlxspath, startDate);
		 Thread.sleep(2000);
		 Reporter.log("After book meeting - Check payment part with database");
		 paymentVarificationWithDatabase.bookNormalMeeting_ByUser(xlxspath);
		 Thread.sleep(5000);
		 Navigates_Modules.RedirectToUserLogout();
		 Thread.sleep(8000);
		 
		 
		 // Before Accept Meeting By User - User Cancell Meeting From My Invite Receive List
		 Reporter.log("Mentor login and decline user's invitation from my invite receive tab");
		 Navigates_Modules.RedirectToLogin();
		 Thread.sleep(5000);
		 Login_Module.mentorLogin(xlxspath);
		 Thread.sleep(5000);
		 MyAccountMenu.openMentorMyInvite();
		 Thread.sleep(5000);
		 MyInvites.declineInvitationFromReceivedByMentor();
		 Thread.sleep(5000);
		 
		 Reporter.log("After decline meeting check payment with database");
		 // After Cancel Meeting - Check payment verification.
		 
		 paymentVarificationWithDatabase.beforeAcceptNormalInvite_CancellByMentor_CheckPayment(xlxspath);
		 Thread.sleep(5000);
		 Success_FailureMessageSheet.storeMessageinfile("normalInvite_AfterAccept_CancellByUser == Run Successfully...");
		
  }
  
  @AfterClass
  public void afterClass() 
  {
	  driver.quit();
  }

}
