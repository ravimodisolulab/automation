package TestCases_Strict_Policy_ByMentor;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Application_Module.Login_Module;
import Application_Module.MeetingBookingFullFlow;
import Application_Module.MyAccountMenu;
import Application_Module.MyInvites;
import Application_Module.MyMeeting;
import Application_Module.Navigates_Modules;
import Application_Module.launchBrowser;
import Application_Module.paymentVarificationWithDatabase;
import Config.Actions_Class;
import Config.Success_FailureMessageSheet;
import Config.TestData_Sheet;
import Config.generalFunctions;

import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class TC_006_PartialRefundByAdmin extends Actions_Class
{
	  public static String xlxspath = "D:\\Projects\\inputFileFolder\\TestData_Sheet.xlsx";
	  
	  @BeforeClass
	  public void beforeClass() throws Exception 
	  {
		  Success_FailureMessageSheet.storeMessageinfile("**********************************************************");
		  String classname = "****===> No Show By User Scenario <===***";
		  Success_FailureMessageSheet.storeMessageinfile(classname);
		  Success_FailureMessageSheet.storeMessageinfile("**********************************************************");
	      Thread.sleep(5000);
		  launchBrowser.chromeBrowser();
		  Navigates_Modules.openwebsite();
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  Login_Module.WebAuthentication(xlxspath);
		  Thread.sleep(10000);
	  }
	  @Test
	  public void Meeting_Complete_Scenario() throws Exception
	  {
			// User Login, Book Meeting and Logout from website
		     Reporter.log("User login to the system book meeting and logout from system...");
		  	 Navigates_Modules.RedirectToLogin();
			 Thread.sleep(2000);
			 Login_Module.userLogin(xlxspath);
			 Thread.sleep(5000);
			 DataSheetFile(xlxspath, "BookMeeting");
			 int i = TestData_Sheet.getCellDataInt(1, 1);
			 String startDate = generalFunctions.dateclasses(i);
			 Reporter.log("Meeting Booking Date:--" + startDate);
			 Thread.sleep(2000);
			 // Meeting Booking By User
			 
			 MeetingBookingFullFlow.normalMeetingBookingFullFlow(xlxspath, startDate);
			 Thread.sleep(2000);
			 Reporter.log("After book meeting - Check payment part with database");
			 paymentVarificationWithDatabase.bookNormalMeeting_ByUser(xlxspath);
			 Thread.sleep(5000);
			 Navigates_Modules.RedirectToUserLogout();
			 Thread.sleep(8000);
			 
	
			 Reporter.log("Mentor logged in to the system, Accept User's normal invite and cancell it from MY Meeting");
			 // Mentor Login and Navigate to My Invite Screen			 
		  	 Navigates_Modules.RedirectToLogin();
			 Thread.sleep(2000);
			 Login_Module.mentorLogin(xlxspath);
			 Thread.sleep(5000);
			 MyAccountMenu.openMentorMyInvite();
			 MyInvites.acceptInvitationFromReceivedByMentor();
			 Thread.sleep(2000);
			 
			 Reporter.log("Mentor Accept - User's normal invite from My Invite and Check Payment with Database....");
			 paymentVarificationWithDatabase.AcceptNormalInvite_ByMentor_CheckPayment(xlxspath);
			 Thread.sleep(5000);
			 
			 Reporter.log("Call Complete Meeting API");
			 MyMeeting.API_CALL(meetingcompleted);
			 Thread.sleep(5000);
			 Reporter.log("After complete meeting - check payment");
			 paymentVarificationWithDatabase.afterCompleteMeetingSuccessfully_CheckPayment(xlxspath);
			 Thread.sleep(5000);
			 
			 Reporter.log("After complete meeting successfully - CALL Partial Refund By ADMIN API");
			 MyMeeting.API_CALL(partialRefund);
			 Thread.sleep(5000);
			 Reporter.log("After Partial Refund By Admin - Check payment");
			 paymentVarificationWithDatabase.afterNoShowByBoth_CheckPayment(xlxspath);
			 Thread.sleep(5000);
			 
			 MyAccountMenu.openDashboard();
			 Thread.sleep(5000);
			 
	  }
	
	  @AfterClass
	  public void afterClass() 
	  {
		  driver.quit();
	  }

}
